# VLC with built in EAC3 to AC3 filter

The main focus of this project was to make VLC automatically convert a EAC3
audio stream (also known as Dolby Digital Plus) to a AC3 audio stream (also
known as Dolby Digital) when a S/PDIF protocol is used, taking advantage of the
similarity between the two, since EAC3 was built using AC3 as a base but it's
not backwards compatible. At the begining of the project it was determined that
it would be optimal to implement this first as a bitstream filter in ffmpeg, one
of the API libraries that VLC uses. Therefore two repositories/projects are
needed to accomplish this project, one is the ffmpeg repository, that contains
the bitstream filter, and this one, that contains a VLC fork that utilizes this
filter.

The way that VLC uses the bitstream filter is that, when it detects that the
input it's receiving is EAC3 and its output is S/PDIF, it calls the filter to
convert the EAC3 stream to an AC3 stream which does fit inside (or can be
converted to) a S/PDIF stream.

The main code is in modules/audio_filter/converter/eac3toac3.c

## What's left to do

* The filter so far does not take full advantage of the backwards compatibility
between the two codecs, so it behaves more like a light decode and light encode.
* Handling different "special" encoding forms of EAC3. 

## Compilation

First it is necesary to build ffmpeg from my repo to get the bitstream filter:

* Get the ffmpeg source from
    https://code.videolan.org/gsoc/gsoc2020/matiaslgonzalez/ffmpeg-gsoc2020
* Compile it using the normal ffmpeg guide:
    https://trac.ffmpeg.org/wiki/CompilationGuide
    and using the --prefix=/path/to/sources/bin/ffmpeg/ flag
* Then compile VLC using the official guide:
    https://wiki.videolan.org/Category:Building/ and before running the
    "./configure" step run this two commands
```export LD_LIBRARY_PATH=/path/to/sources/bin/ffmpeg/lib/:$LD_LIBRARY_PATH```
```export PKG_CONFIG_PATH=/path/to/sources/bin/ffmpeg/lib/pkgconfig/:$PKG_CONFIG_PATH```
* After that VLC should run normally

## Special thanks
* François Cartegnie, my main mentor
* Jean-Baptiste Kempf, my secondary mentor
* Lynne from the ffmpeg team, my spiritual mentor
