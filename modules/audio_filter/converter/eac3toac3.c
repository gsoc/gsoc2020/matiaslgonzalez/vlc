/* eac3toac3.c */

/*****************************************************************************
 * Preamble
 *****************************************************************************/

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif
#include <assert.h>

#include <libavcodec/avcodec.h>

#include <vlc_common.h>
#include <vlc_plugin.h>
#include <vlc_aout.h>
#include <vlc_block.h>
#include <vlc_filter.h>

/*****************************************************************************
 * Module descriptor
 *****************************************************************************/
static int Open(vlc_object_t *);

vlc_module_begin()
    set_description(N_("Converter for EAC3 to AC3"))
        set_category(CAT_AUDIO)
            set_subcategory(SUBCAT_AUDIO_MISC)
                set_capability("audio converter", 1)
                    set_callback(Open)
                        vlc_module_end()

    /*****************************************************************************
 * Local prototypes
 *****************************************************************************/
    static block_t *DoWork(filter_t *p_filter, block_t *p_in_buf);

static int Open(vlc_object_t *object)
{
    filter_t *filter = (filter_t *)object;

    const es_format_t *src = &filter->fmt_in;
    es_format_t *dst = &filter->fmt_out;

    if (src->i_codec != VLC_CODEC_EAC3 || (dst->i_codec != VLC_CODEC_SPDIFL && dst->i_codec != VLC_CODEC_SPDIFB))
        return VLC_EGENERIC;

    filter->pf_audio_filter = DoWork;
    if (filter->pf_audio_filter == NULL)
        return VLC_EGENERIC;

    return VLC_SUCCESS;
}

static block_t *DoWork(filter_t *p_filter, block_t *p_in_buf)
{
    const AVBitStreamFilter *filter = av_bsf_get_by_name("eac3toac3");
    AVBSFContext *bsfc;
    AVPacket *pkt = NULL;
    int ret = 0;
    block_t *p_out_buf = NULL;

    ret = av_new_packet(pkt, p_in_buf->i_buffer);
    if (ret)
        return NULL;

    memcpy(&pkt->data, &p_in_buf->p_buffer, p_in_buf->i_buffer);

    ret = av_bsf_alloc(filter, &bsfc);
    if (ret){
        av_packet_unref(pkt);
        return NULL;
    }

    ret = av_bsf_init(bsfc);
    if (ret)
        return NULL;

    ret = av_bsf_send_packet(bsfc, pkt);
    if (!ret) /* ret == 0 on success */
    {
        av_bsf_receive_packet(bsfc, pkt);
        p_out_buf = block_Alloc(pkt->size);
        memcpy(&p_in_buf->p_buffer, &pkt->data, pkt->size);
        block_CopyProperties(p_out_buf, p_in_buf);
        p_out_buf->i_buffer = pkt->size;
        block_Release(p_in_buf);
    }
    av_packet_unref(pkt);
    av_bsf_free(&bsfc);

    return p_out_buf;
}